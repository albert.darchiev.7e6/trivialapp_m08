package com.example.trivialapp.model

    data class User(var name: String, var bestScore: Int)
