package com.example.trivialapp

object QuestionsLists{
    val historyQ: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf("¿En qué año llegó el hombre a la Luna?", "c_1969", "f_1967", "f_1961", "f_1962"),
        mutableListOf("¿Cuál era la ciudad hogar de Marco Polo?", "f_Barcelona", "f_Florencia", "c_Venecia", "f_Roma"),
        mutableListOf("¿De qué país se independizó Eslovenia?","c_Yugoslavia", "f_Ljubljana", "f_Triglav", "f_Bohnij"),
        mutableListOf("¿Como se  a la traductora del conquistador Hernán Cortés en tierras aztecas?", "f_Quetzalli", "c_Malinche", "f_Tlalocan", "f_Sihuca"),
        mutableListOf("¿En qué año se disolvió la Unión Soviética?", "f_1992", "f_1994", "f_1989", "c_1991"),
        mutableListOf("¿Quién era el emperador de Roma cuando murió Jesús de Nazaret?", "c_Tiberio", "f_Julio César", "f_Augustus", "f_Zeus"),
        mutableListOf("¿Qué exploradores fueron los primeros en dar la vuelta al mundo?", "c_Magallanes y Elcano", "f_Elcano y Carlos", "f_Thevet y Carlos", "f_Magallanes y Carlos"),
        mutableListOf("¿Cuándo se creó la ONU?", "f_1955", "c_1945", "f_1965", "f_1968"),
        mutableListOf("¿Quién fue el primer presidente de Estados Unidos?", "f_Barack Obama", "c_George Washington", "f_John Kennedy", "f_Thomas Jefferson"),
        mutableListOf("¿Quiénes fundaron la ciudad de Roma según la leyenda?", "f_Astérix y Obélix", "c_Rómulo y Remo", "f_Lucio y Tarquinio", "f_Rea y Marte"),
        mutableListOf("Marrakech es la capital de Marruecos", "VF", "FALSO", "", ""),
        mutableListOf("El Príncipe Enrique es más alto que el Príncipe Guillermo", "VF", "FALSO", "", ""),
        mutableListOf("Cleopatra era de ascendencia egipcia", "VF", "FALSO", "", ""),
        mutableListOf("¿Napoleón Bonaparte fue coronado emperador de Francia en 1804?", "VF","VERDADERO", "", ""),
        mutableListOf("¿La Primera Guerra Mundial comenzó en 1914?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El Imperio Romano fue fundado en el año 476 d.C.?", "VF","FALSO", "", ""),
        mutableListOf("¿La Revolución Industrial comenzó en Inglaterra en el siglo XVIII?", "VF","VERDADERO", "", ""),
        mutableListOf("¿La Declaración de Independencia de Estados Unidos fue firmada en 1776?", "VF","VERDADERO", "", "")
    )

    val artAndLiteratureQ: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf("¿Cuál era el nombre artístico con el que firmaba Vincent Van Gogh sus obras?","f_Gogh", "c_Vincent ", "f_VanG", "f_VGogh" ),
        mutableListOf("¿Cuál es el libro más largo jamás publicado?", "f_Límite, Frank Schatzing", "f_Viaje al oeste", "c_En busca del tiempo perdido", "f_La novela de Genji"),
        mutableListOf("Notre Dame es una catedral con un estilo arquitectónico muy definido. ¿Cuál?","f_Clásico", "f_Jónico", "c_Gótico", "f_Romano"),
        mutableListOf("¿Quién es al autor de la obra Guernica?", "f_Van Gogh", "c_Pablo Picasso", "f_Johannes Vermeer", "f_Safo de Mitilene"),
        mutableListOf("¿Quién es el autor del libro Poeta en Nueva York?", "c_ Federico García Lorca", "f_George Orwell", "f_Miguel de Cervantes", "f_ Camilo José Cela"),
        mutableListOf("¿Cuáles son las flores más famosas pintadas por Van Gogh?", "c_Girasoles", "f_Flor de loto", "f_Rosas", "f_Margaritas"),
        mutableListOf("¿En qué museo puedes contemplar la mayor colección de obras de Kandinsky?", "c_Museo de Arte Moderno de Nueva York", "f_Louvre de París", "f_Hermitage", "f_Museo Británico"),
        mutableListOf("¿En qué país surgió el art decó?", "f_Francia", "c_Estados Unidos", "f_Reino Unido", "f_España"),
        mutableListOf("¿Cuáles son los tres órdenes clásicos de la arquitectura clásica?", "c_Dórico, jónico y corintio", "f_Mudéjar, neomudéjar y mozárabe", "f_Masamento, fuste y capitel", "f_Fuste, capitel y Cortino"),
        mutableListOf("Quién pintó el cuadro \"El matrimonio Arnolfini\"", "f_El Bosco.", "f_Roger van der Wayden", "f_Rembrandt", "c_Jan Van Eyck"),
        mutableListOf("¿Quién escribió el libro 'Don Quijote de la Mancha'?", "c_Miguel de Cervantes", "f_Jorge Luis Borges", "f_Gabriel García Márquez", "f_Ernest Hemingway"),
        mutableListOf("¿Cuál es el nombre de la última obra de arte pintada por Leonardo da Vinci?", "c_La última cena", "f_La Mona Lisa", "f_El descubrimiento de América", "f_La noche estrellada"),
        mutableListOf("¿Qué pintor es conocido por su serie de cuadros 'Las Meninas'?",  "f_Francisco de Goya", "c_Diego Velázquez","f_Pablo Picasso", "f_Salvador Dalí"),
        mutableListOf("¿Qué poeta es considerado el padre del romanticismo en España?", "c_José de Espronceda", "f_Federico García Lorca", "f_Antonio Machado", "f_Gabriel José de la Concordia García Márquez"),
        mutableListOf("¿Qué escritor es conocido por sus obras 'Cien años de soledad' y 'El amor en los tiempos del cólera'?", "c_Gabriel García Márquez", "f_Jorge Luis Borges", "f_Mario Vargas Llosa", "f_Ernesto Sabato"),
        mutableListOf("¿El pintor español Salvador Dalí es conocido por su estilo surrealista?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El escritor estadounidense Ernest Hemingway ganó el premio Nobel de Literatura en 1954?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El poeta y escritor ruso Alexander Pushkin es considerado el padre de la literatura rusa moderna?", "VF","VERDADERO", "", ""),
        mutableListOf("¿La novela Cien años de soledad de Gabriel García Márquez fue publicada en el año 1970?", "VF","FALSO", "", ""),
        mutableListOf("¿La escultura David de Miguel Ángel es considerada una de las obras más icónicas de la historia del arte?", "VF","VERDADERO", "", "")

    )

    val sportQ: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf("¿Cuándo se celebró el primer mundial de fútbol?","f_1940", "f_1950 ", "f_1945", "c_1930"),
        mutableListOf("¿En qué club italiano jugó Diego Maradona?", "c_Nápoli", "f_Cremonese", "f_Atlanta", "f_Juventus"),
        mutableListOf("¿Qué revista concede el Balón de Oro?","f_Spain Football", "f_Brazil Football", "c_France Football", "f_Italia Football"),
        mutableListOf("¿Qué país ha ganado más Eurocopas seguidas?", "f_Francia", "c_España", "f_Brasil", "f_Portugal"),
        mutableListOf("¿Quién es el ganador más exitoso de la Copa del Mundo de Fútbol?",  "f_Argentina", "f_Alemania", "c_Brasil","f_Italia"),
        mutableListOf("¿Quién es el jugador más exitoso en la historia de la NBA?", "f_Kobe Bryant", "f_LeBron James", "f_Magic Johnson", "c_Michael Jordan"),
        mutableListOf("¿Quién es el boxeador más exitoso de todos los tiempos?", "c_Muhammad Ali", "f_Mike Tyson", "f_Joe Frazier", "f_George Foreman"),
        mutableListOf("¿Qué equipo ganó la Super Bowl LVI?",  "f_Kansas City Chiefs", "f_Green Bay Packers", "f_New England Patriots","c_Tampa Bay Buccaneers"),
        mutableListOf("¿Quién es el tenista más exitoso de la historia?", "c_Roger Federer", "f_Rafael Nadal", "f_Novak Djokovic", "f_Pete Sampras"),
        mutableListOf("¿Quién es el ganador del Tour de France más exitoso?",  "f_Lance Armstrong", "f_Miguel Indurain", "f_Bernard Hinault","c_Eddy Merckx"),
        mutableListOf("¿Quién es el ganador más exitoso de la Fórmula Uno?",  "f_Ayrton Senna", "c_Michael Schumacher","f_Juan Manuel Fangio", "f_Alain Prost"),
        mutableListOf("¿Qué equipo ganó la Copa del Rey de fútbol en 2021?", "c_Athletic Club", "f_Barcelona", "f_Real Madrid", "f_Sevilla"),
        mutableListOf("¿Quién ganó el trofeo de MVP de la NFL en 2021?",  "f_Tom Brady", "f_Patrick Mahomes", "c_Aaron Rodgers","f_Dak Prescott"),
        mutableListOf("¿Quién es el ganador más exitoso del Masters de Augusta?", "c_Jack Nicklaus", "f_Tiger Woods", "f_Arnold Palmer", "f_Gary Player"),
        mutableListOf("¿El equipo de fútbol Manchester United es originario de Manchester, Inglaterra?", "VF","VERDADERO", "", ""),
        mutableListOf("¿Michael Phelps es el nadador con más medallas de oro en Juegos Olímpicos?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El baloncesto fue inventado por James Naismith en 1891?", "VF","VERDADERO", "", ""),
        mutableListOf("¿Lionel Messi es jugador de fútbol originario de Brasil?", "VF","FALSO", "", ""),
        mutableListOf("¿El hockey sobre hielo es un deporte olímpico?", "VF","VERDADERO", "", "")
    )

    val geografyQ: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf("¿Cuál es el río más largo de América?", "c_Amazonas", "f_Mississippi", "f_Río de la Plata", "f_Nilo"),
        mutableListOf("¿En qué océano se encuentra la isla de Hawaii?", "f_Atlántico", "c_Pacífico", "f_Índico", "f_Antártico"),
        mutableListOf("¿Cuál es la ciudad más poblada de África?", "f_El Cairo", "f_Johannesburgo", "c_Lagos", "f_Kinshasa"),
        mutableListOf("¿Cuál es el país más grande del mundo en términos de superficie?", "f_Canadá", "c_Rusia", "f_China", "f_Estados Unidos"),
        mutableListOf("¿Cuál es el punto más alto de Europa?", "f_El Monte Blanc", "f_El Monte Olimpo", "c_El Monte Elbrus", "f_El Monte Veliko Tarnovo"),
        mutableListOf("¿En qué mar se encuentra la isla de Creta?", "f_Adriático", "c_Mediterráneo", "f_Egeo", "f_Negro"),
        mutableListOf("¿Cuál es la capital de Australia?", "f_Sydney", "f_Melbourne", "c_Canberra", "f_Perth"),
        mutableListOf("¿Cuál es el río más largo del mundo?", "f_Amazonas", "c_Nilo", "f_Misisipi", "f_Yangtsé"),
        mutableListOf("¿En qué océano se encuentra la isla de Madagascar?", "f_Atlántico", "f_Pacífico", "c_Índico", "f_Artico"),
        mutableListOf("¿Cuál es la ciudad más poblada de América?", "f_Mexico DF", "f_Buenos Aires", "c_Sao Paulo", "f_Rio de Janeiro"),
        mutableListOf("¿En qué país se encuentra el Monte Everest?", "f_India", "f_Tibet", "c_Nepal", "f_Bhutan"),
        mutableListOf("¿Cuál es la capital de Rusia?", "f_San Petersburgo", "f_Volgogrado", "c_Moscú", "f_Kiev"),
        mutableListOf("¿En qué océano se encuentra la isla de Nueva Zelanda?", "f_Atlántico", "f_Mediterráneo", "c_Pacífico", "f_Ártico"),
        mutableListOf("¿Cuál es la capital de Brasil?", "f_Rio de Janeiro", "c_Brasilia", "f_Sao Paulo", "f_Belo Horizonte"),
        mutableListOf("¿En qué país se encuentra la ciudad de Beijing?", "c_China", "f_Japón",  "f_Corea del Sur",),
        mutableListOf("¿La Amazonia es el segundo rio más largo del mundo?", "VF","FALSO", "", ""),
        mutableListOf("¿La isla de Madagascar se encuentra en el océano Pacífico?", "VF","FALSO", "", ""),
        mutableListOf("¿La ciudad más poblada de África es Lagos?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El Monte Elbrus es el punto más alto de Europa?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El Mar Mediterráneo es el mar más grande del mundo?", "VF","FALSO", "", "")
    )

    val cinemaQ: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf("¿Quién dirigió la película The Shawshank Redemption?", "f_Martin Scorsese", "f_Quentin Tarantino", "c_Frank Darabont", "f_Christopher Nolan"),
        mutableListOf("¿Qué actriz interpreta a Katniss Everdeen en la saga de Los Juegos del Hambre?", "f_Emma Watson", "f_Martina Scorsese", "c_Jennifer Lawrence", "f_Daisy Ridley"),
        mutableListOf("¿Quién es el actor que interpreta al personaje de Tony Stark/Iron Man en la saga de Marvel?", "f_Chris Evans", "f_Daisy Ridley", "c_Robert Downey Jr.", "f_Chris Hemsworth"),
        mutableListOf("¿Qué película ganó el Oscar a Mejor Película en 2020?", "f_Joker", "f_Batman", "c_Parasite", "f_1917"),
        mutableListOf("¿Quién es el director de la saga de Harry Potter?", "f_Christopher Nolan", "f_Martin Scorsese", "f_Quentin Tarantino", "c_David Yates"),
        mutableListOf("¿Quién es el actor que interpreta al personaje de Sherlock Holmes en la serie de Sherlock?", "f_Chris Evans", "c_Benedict Cumberbatch", "f_Martin Freeman", "f_Robert Downey Jr."),
        mutableListOf("¿Qué banda compuso la banda sonora de la película La La Land?", "f_U2", "f_Coldplay", "c_Justin Hurwitz", "f_Adele"),
        mutableListOf("¿Qué película ganó el premio a Mejor Película en los premios de la Academia en 2018?", "f_Replicas", "c_La Forma del Agua", "f_Dunkirk", "f_Tres Anuncios en las Afueras"),
        mutableListOf("¿Quién es el actor que interpreta al personaje de Jack Sparrow en la saga de Piratas del Caribe?", "f_David Yates", "c_Johnny Depp", "f_Orlando Bloom", "f_Keira Knightley"),
        mutableListOf("¿Qué serie de televisión ganó el premio a Mejor Serie Dramática en los premios Emmy en 2019?", "f_Vikingos", "c_Game of Thrones", "f_The Crown", "f_Westworld"),
        mutableListOf("¿Quién es el actor que interpreta al personaje de James Bond en la saga de James Bond?", "f_Roger Craig", "c_Daniel Craig", "f_Sean Connery", "f_Roger Moore"),
        mutableListOf("¿Qué película ganó el premio a Mejor Película Extranjera en los premios de la Academia en 2019?", "f_Atenas", "c_Roma", "f_La Favorita", "f_Bohemian Rhapsody"),
        mutableListOf("¿Quentin Tarantino dirigió la película La La Land?", "VF","FALSO", "", ""),
        mutableListOf("¿La actriz Emma Stone ganó un Oscar por su papel en la película La La Land?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El actor Tom Hanks ha ganado dos premios Oscar a Mejor Actor?", "VF","VERDADERO", "", ""),
        mutableListOf("¿La película Titanic ganó el premio a Mejor Película en los premios de la Academia en 1997?", "VF","VERDADERO", "", ""),
        mutableListOf("¿La serie Breaking Bad ganó el premio a Mejor Serie Dramática en los premios Emmy en 2010?", "VF","FALSO", "", "")
    )

    val gamesQ: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf("¿Quién es el creador del videojuego Super Mario Bros?", "f_Hideo Kojima", "c_Shigeru Miyamoto","f_Tetsuya Nomura", "f_Yoichi Wada"),
        mutableListOf("¿Qué consola de videojuegos fue la primera en tener un procesador de 32 bits?",  "f_PlayStation", "f_Nintendo 64","c_Sega Saturn", "f_Dreamcast"),
        mutableListOf("¿Qué videojuego de rol es considerado como el padre de los videojuegos de rol en occidente?",  "f_Final Fantasy", "f_Dragon Quest","c_The Elder Scrolls: Arena", "f_World of Warcraft"),
        mutableListOf("¿Quién es el creador del videojuego Minecraft?", "c_ Markus Persson", "f_Shigeru Miyamoto", "f_Hideo Kojima", "f_Tetsuya Nomura"),
        mutableListOf("¿Qué videojuego de acción en primera persona fue el primer juego en utilizar el motor de juego Source?", "c_Half-Life 2", "f_Portal", "f_Left 4 Dead", "f_Team Fortress 2"),
        mutableListOf("¿Quién es el creador de la saga de videojuegos de Assassin's Creed?",  "f_Shigeru Miyamoto", "c_Patrice Désilets","f_Hideo Kojima", "f_Tetsuya Nomura"),
        mutableListOf("¿Qué videojuego de estrategia en tiempo real es considerado como uno de los juegos más influyentes y exitosos de la historia?", "f_Warcraft III", "f_Command & Conquer", "f_Age of Empires", "c_Starcraft"),
        mutableListOf("¿Qué videojuego de lucha es considerado como el primer juego de lucha de la historia?", "c_Street Fighter", "f_Mortal Kombat", "f_Tekken", "f_Super Smash Bros."),
        mutableListOf("¿Qué videojuego de aventura gráfica es considerado como uno de los juegos más influyentes y exitosos de la historia?",  "f_King's Quest", "f_Indiana Jones and the Fate of Atlantis", "c_The Secret of Monkey Island","f_Grim Fandango"),
        mutableListOf("¿Qué videojuego de plataformas es considerado como uno de los juegos más influyentes y exitosos de la historia?", "c_Super Mario Bros.", "f_Sonic the Hedgehog", "f_Mega Man", "f_Castlevania"),
        mutableListOf("¿Qué videojuego es desarrollado por Ubisoft?", "f_Call of Duty", "f_Destiny", "f_Overwatch", "c_Assassin's Creed"),
        mutableListOf("Volition es el desarrollador de Saits Row","VF","VERDADERO",""),
        mutableListOf("¿El videojuego Mario Bros. fue creado por Nintendo?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El videojuego Grand Theft Auto V fue lanzado en el año 2000?", "VF","FALSO", "", ""),
        mutableListOf("¿El videojuego The Last of Us fue desarrollado por Naughty Dog?", "VF","VERDADERO", "", ""),
        mutableListOf("¿El videojuego Doom fue lanzado en el año 2016?", "VF","FALSO", "", ""),
        mutableListOf("¿El videojuego Minecraft fue desarrollado por Mojang Studios?", "VF","VERDADERO", "", "")
    )
}
