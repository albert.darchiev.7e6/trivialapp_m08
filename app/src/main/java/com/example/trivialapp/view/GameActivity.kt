package com.example.trivialapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.trivialapp.R

class GameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, Fragment1())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()

        }

    }

    @Override
    override fun onBackPressed() {
        //super.onBackPressed()
    }

}