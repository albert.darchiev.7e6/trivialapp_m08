package com.example.trivialapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trivialapp.R
import com.example.trivialapp.databinding.FragmentSelectTopicBinding

class FragmentSelectTopic : Fragment() {
    companion object{
        var topic = 0

    }
    lateinit var binding: FragmentSelectTopicBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSelectTopicBinding.inflate((layoutInflater))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.historyButton.setOnClickListener {
            topic = 1
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.sportButton.setOnClickListener {
            topic = 2
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.artButton.setOnClickListener {
            topic = 3
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.cinemaButton.setOnClickListener {
            topic = 4
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.geografyButton.setOnClickListener {
            topic = 5
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.gamesButton.setOnClickListener {
            topic = 6
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.goBackButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGamemodes())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }
}