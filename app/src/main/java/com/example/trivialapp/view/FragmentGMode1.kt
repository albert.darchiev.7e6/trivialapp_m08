package com.example.trivialapp.view
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.trivialapp.view.FragmentGamemodes.Companion.gameMode
import com.example.trivialapp.view.FragmentSelectTopic.Companion.topic
import com.example.trivialapp.QuestionsLists.artAndLiteratureQ
import com.example.trivialapp.QuestionsLists.cinemaQ
import com.example.trivialapp.QuestionsLists.gamesQ
import com.example.trivialapp.QuestionsLists.geografyQ
import com.example.trivialapp.QuestionsLists.historyQ
import com.example.trivialapp.QuestionsLists.sportQ
import com.example.trivialapp.R
import com.example.trivialapp.databinding.FragmentGMode1Binding
import com.example.trivialapp.viewmodel.gameViewModel

class FragmentGMode1 : Fragment(), View.OnClickListener {

    lateinit var binding: FragmentGMode1Binding
    companion object {
        var totalScore = 0
        var round = 0
        var lives = -1
        val allQuestions = mutableListOf(historyQ, artAndLiteratureQ, sportQ, geografyQ, cinemaQ, gamesQ)
        var question: List<MutableList<String>> = mutableListOf()
        var livesList = mutableListOf<ImageView?>()
        var randomQuestionsOrder = allQuestions.shuffled()
        var randTopic = 0
    }

    // BARRA DE CUENTA ATRAS
    val timer = object : CountDownTimer(10000, 100) {
        override fun onTick(millisUntilFinished: Long) {
            binding.countDownProgressBar.progress -= 1
        }
        override fun onFinish() {
            showCorrect()
            delay(1300)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        randomQuestionsOrder = allQuestions.shuffled()
        totalScore = 0
        round = 0
        super.onCreate(savedInstanceState)
        binding = FragmentGMode1Binding.inflate((layoutInflater))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userViewModel = ViewModelProvider(requireActivity()).get(gameViewModel::class.java) //
        lives = -1
        livesList = mutableListOf(binding.live1, binding.live2, binding.live3, binding.live4, binding.live5)
        question = mutableListOf()
        when (gameMode){
            1-> question = allQuestions.random().shuffled()
            2-> when (topic){
                1-> question = historyQ.shuffled()
                2-> question = sportQ.shuffled()
                3-> question = artAndLiteratureQ.shuffled()
                4-> question = cinemaQ.shuffled()
                5-> question = geografyQ.shuffled()
                6-> question = gamesQ.shuffled()
            }
            3-> {
                binding.topicRoundTextView?.isVisible = true
                question = randomQuestionsOrder[5]
                //question = allQuestions.random().shuffled()
                livesList.forEachIndexed() { index, value ->
                    value!!.isVisible = true
                }
            }
        }
        updateInfo()
        binding.aButton.setOnClickListener(this)
        binding.bButton.setOnClickListener(this)
        binding.cButton.setOnClickListener(this)
        binding.dButton.setOnClickListener(this)
        /*
        binding.dButton.setOnClickListener { // TEST VIEW MODEL
            userViewModel.updateInfo2()
        }
        */

    }




    override fun onClick(v: View?) {
        val button = v as Button
        val tag = button.tag.toString().toInt()
        if (question[round][tag].split("_")[0] == "c" || question[round][2] == button.text) {
            disableButton()
            totalScore += binding.countDownProgressBar.progress
            binding.scoreTextView.text = "Score: $totalScore"
            button.setBackgroundColor(Color.GREEN)
        }
        else {
            button.setBackgroundColor(Color.RED)
            showCorrect()
            if (gameMode ==3){
                lives++
                livesList[lives]!!.isVisible = false

            }
        }
        timer.cancel()
        delay(1300)
    }


    fun showCorrect(){
        disableButton()
        if (questionType()) {
            if (question[round][1].split("_")[0] == "c") binding.aButton.setBackgroundColor(Color.GREEN)
            if (question[round][2].split("_")[0] == "c") binding.bButton.setBackgroundColor(Color.GREEN)
            if (question[round][3].split("_")[0] == "c") binding.cButton.setBackgroundColor(Color.GREEN)
            if (question[round][4].split("_")[0] == "c") binding.dButton.setBackgroundColor(Color.GREEN)
        }
        else{
            if (question[round][2] == binding.bButton.text) binding.bButton.setBackgroundColor(Color.GREEN)
            if (question[round][3] == binding.cButton.text) binding.cButton.setBackgroundColor(Color.GREEN)
        }
    }
    fun updateInfo(){
        binding.countDownProgressBar.progress = 100
        if (gameMode !=3){
            binding.roudTextView.text = "Round ${round + 1}/10"
            binding.scoreTextView.text = "Score: $totalScore"
        }
        else {
            binding.countDownProgressBar.isVisible = false
            binding.roudTextView.text = "Round ${round + 1}/15"
            binding.scoreTextView.isVisible = false
        }

        if (questionType()){
            binding.aButton.isInvisible = false
            binding.dButton.isInvisible = false
            binding.aButton.text = question[round][1].split("_")[1]
            binding.bButton.text = question[round][2].split("_")[1]
            binding.cButton.text = question[round][3].split("_")[1]
            binding.dButton.text = question[round][4].split("_")[1]
        }
        else {
            binding.aButton.isInvisible = true
            binding.bButton.text = "VERDADERO"
            binding.cButton.text = "FALSO"
            binding.dButton.isInvisible = true
        }
        binding.aButton.isEnabled = true
        binding.bButton.isEnabled = true
        binding.cButton.isEnabled = true
        binding.dButton.isEnabled = true
        binding.questionTextView.text = question[round][0]
        binding.aButton.setBackgroundColor(Color.MAGENTA)
        binding.bButton.setBackgroundColor(Color.MAGENTA)
        binding.cButton.setBackgroundColor(Color.MAGENTA)
        binding.dButton.setBackgroundColor(Color.MAGENTA)
        if (gameMode == 3)timer.cancel()
        else timer.start()
    }

    fun questionType(): Boolean{
        return question[round][1] != "VF" //Si es tipo Verdadero/Falso es FALSE
    }

    fun disableButton(){
        binding.aButton.isEnabled = false
        binding.bButton.isEnabled = false
        binding.cButton.isEnabled = false
        binding.dButton.isEnabled = false
    }
    fun delay(miliSec: Int) {
        timer.cancel()
        round++
        Handler(Looper.getMainLooper()).postDelayed({

            if (round == 10 && gameMode != 3) changeFragment()
            else if (gameMode == 3 && (lives == 4 || round == 60)) changeFragment()
            else if (gameMode == 3 && round != 0 &&  randTopic != 5){
                if (round ==10 && randTopic != 5) {
                    randTopic++

                }
                if (round == 10){
                    question = randomQuestionsOrder[randTopic]
                    round = 0
                }
                binding.topicRoundTextView?.text = "TOPIC: ${randTopic+1}/6"
                updateInfo()
            }
            else if (randTopic == 5 && round == 10) changeFragment()
            else updateInfo()
        }, miliSec.toLong())
    }

    fun changeFragment(){
        timer.cancel()
        round = 0
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, FragmentResultScreen())
            setReorderingAllowed(true)
            addToBackStack("name")
            commit()
        }
    }
 }
