package com.example.trivialapp.view

import android.app.Activity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.trivialapp.R
import com.example.trivialapp.databinding.FragmentRankingBinding
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.io.PrintWriter

class RankingFragment : Fragment() {
    lateinit var binding: FragmentRankingBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRankingBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val appContext = requireContext().applicationContext

        var orderedRanking = readFile().split(";") as MutableList<String>
        orderedRanking.removeAt(orderedRanking.size-1)
        orderedRanking.sortByDescending {it.split("|")[1].toInt()}

        var usersList = mutableListOf(binding.player1, binding.player2, binding.player3, binding.player4, binding.player5, binding.player6, binding.player7, binding.player8, binding.player9, binding.player10)
        for (i in 0 until orderedRanking.size){
            val user = orderedRanking[i].split("|")
            //usersList[i].gravity = Gravity.LEFT
            usersList[i].isVisible = true
            usersList[i].text = "${user[0]} : ${user[1]}"
        }

        //Toast.makeText(appContext, orderedRanking.toString(), Toast.LENGTH_LONG).show()

        binding.goBackButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, Fragment1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }

    fun readFile(): String {
        var content = ""
        var rankingList = ("")
        val file = InputStreamReader(requireActivity().openFileInput("rankingFile.txt"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){

            rankingList += line
            content += line + "\n"
            line = br.readLine()
        }
        //binding.player10.text = content
        return (rankingList)
    }




}