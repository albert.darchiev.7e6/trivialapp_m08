package com.example.trivialapp.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.trivialapp.R
import com.example.trivialapp.databinding.Fragment1Binding
import java.io.BufferedReader
import java.io.InputStreamReader

class Fragment1: Fragment() {
    lateinit var binding: Fragment1Binding
    companion object{
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = Fragment1Binding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        val appContext = requireContext().applicationContext
        val anim = AnimationUtils.loadAnimation(context,R.anim.rotate_animation)
        val fadeAnim = AnimationUtils.loadAnimation(context,R.anim.fade_out)

        binding.playButton.isEnabled = true

        binding.playButton.setOnClickListener {
            binding.playButton.isEnabled = false
            binding.rankingButton.isEnabled = false
        binding.logoImageView.startAnimation(anim)
        binding.playButton.startAnimation(fadeAnim)
        binding.rankingButton.startAnimation(fadeAnim)



            Handler(Looper.getMainLooper()).postDelayed({
                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, FragmentGamemodes())
                    setReorderingAllowed(true)
                    addToBackStack("name")
                    commit()
                }
            }, 4000.toLong())
        }

        binding.rankingButton.setOnClickListener {
            if (readFile() == "") Toast.makeText(appContext, "No hay usuarios registrados", Toast.LENGTH_SHORT).show()
            else{
                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, RankingFragment())
                    setReorderingAllowed(true)
                    addToBackStack("name")
                    commit()
                }
            }
        }
        }
    fun readFile(): String {
//        var content = ""
        var rankingList = ("")
        val file = InputStreamReader(requireActivity().openFileInput("rankingFile.txt"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){

            rankingList += line
//            content += line + "\n"
            line = br.readLine()
        }
        //binding.player10.text = content
        return (rankingList)
    }
    }



