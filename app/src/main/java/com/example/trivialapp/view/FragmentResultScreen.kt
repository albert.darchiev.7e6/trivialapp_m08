package com.example.trivialapp.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import com.example.trivialapp.R
import com.example.trivialapp.view.FragmentGMode1.Companion.totalScore
import com.example.trivialapp.databinding.FragmentResultScreenBinding
import com.example.trivialapp.model.usersRanking
import com.example.trivialapp.view.FragmentGamemodes.Companion.gameMode
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter

class FragmentResultScreen : Fragment() {
    lateinit var binding: FragmentResultScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentResultScreenBinding.inflate((layoutInflater))
        return binding.root    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.totalScoreTextView.text = "YOUR SCORE: $totalScore"

        binding.fadeScreenImage.isVisible = false
        binding.usernameInsertTextview.isVisible = false
        binding.userEditText.isVisible = false
        binding.saveButton.isVisible = false


        binding.saveScoreButton.isVisible = gameMode == 1
        var gamemodeName =""
        when(gameMode){
            1-> gamemodeName ="CLÁSICO"
            2-> gamemodeName ="CATEGORIAS"
            3-> gamemodeName ="SUPERVIVENCIA"
        }

        binding.shareButton.setOnClickListener {
            val t1 = "YOUR SCORE IS: $totalScore\n " +
                     "IN MODE: $gamemodeName"
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type="text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, t1)
            startActivity(Intent.createChooser(shareIntent,"Share via"))
        }
        binding.returnMenuButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGamemodes())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.saveScoreButton.setOnClickListener{
            binding.fadeScreenImage.isVisible = true
            binding.usernameInsertTextview.isVisible = true
            binding.userEditText.isVisible = true
            binding.saveButton.isVisible = true
            binding.saveScoreButton.isVisible = false
            val appContext = requireContext().applicationContext

            binding.saveButton.setOnClickListener {
                var text = binding.userEditText.text
                if (";" in text || " " in text || "|" in text ||text.length > 7) {
                    Toast.makeText(appContext, "NOMBRE INVÁLIDO", Toast.LENGTH_SHORT).show()
                }

                else{
                    val file = OutputStreamWriter(requireActivity().openFileOutput("rankingFile.txt", Activity.MODE_APPEND))
                    file.appendLine("$text|$totalScore;")
                    file.flush()
                    file.close()

                    Toast.makeText(appContext, "PUNTUACIÓN GUARDADA", Toast.LENGTH_SHORT).show()
                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentContainerView, FragmentGamemodes())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }

            }
    }


}

}