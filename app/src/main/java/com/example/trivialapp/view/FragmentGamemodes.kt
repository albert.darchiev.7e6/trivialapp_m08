package com.example.trivialapp.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.trivialapp.R
import com.example.trivialapp.databinding.FragmentGamemodesBinding
import com.example.trivialapp.view.FragmentGMode1.Companion.question

//data class Question (val statement: String, val correctAnswer: String, val wrongAnswer1: String, val wrongAnswer2: String, val wrongAnswer3: String)

class FragmentGamemodes : Fragment() {
companion object{
    var gameMode = 0
}

    lateinit var binding: FragmentGamemodesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentGamemodesBinding.inflate(layoutInflater)
        question = mutableListOf()
        val anim = AnimationUtils.loadAnimation(context,R.anim.inverserotate_animation)
        val fadeAnim = AnimationUtils.loadAnimation(context,R.anim.fade_in)

        binding.wheelImageImageView.startAnimation(anim)
        binding.fadeScreenImage.isVisible = true
        binding.fadeScreenImage.startAnimation(fadeAnim)
        Handler(Looper.getMainLooper()).postDelayed({
            binding.fadeScreenImage.isVisible = false
            binding.fadeScreenImage.clearAnimation()
        }, 6000.toLong())


        //val questionList = listOf<Question>( Question("Who are you?", "1", "2", "3", "4"),)
        return binding.root
     }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        question = mutableListOf()
        binding.gamemode1Button.setOnClickListener {
            binding.wheelImageImageView.clearAnimation()
            gameMode = 1
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }

        binding.gamemode2Button.setOnClickListener {
            gameMode = 2
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentSelectTopic())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.gamemode3Button.setOnClickListener {
            gameMode = 3
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FragmentGMode1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }

        binding.goBackButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, Fragment1())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }


    }
}

